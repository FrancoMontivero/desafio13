const faker = require('faker');
const { isExportDeclaration } = require('typescript');

const {
	getProducts,
	getProduct,
	addProduct,
	updateProduct,
	removeProduct
} = require('../src/controllers/products')

faker.locale = 'es';

describe("controllers products", () => {
    
    const products = [];

    for(let i = 0 ; i < 25 ; i ++) {
        products.push({
            id: i + 1,
            title: faker.commerce.product(),
            price: parseFloat(faker.commerce.price()),
            thumbnail: faker.image.imageUrl()
        })
    }

    it("getProducts: should are empty when not loading products", () => {
        expect(getProducts()).toEqual([]);
    });

    describe("controller: addProduct", () => {
        it("the response should be a error when parameters are missing", () => {
            expect.assertions(3);
            try { addProduct(faker.commerce.product(), parseFloat(faker.commerce.price())) }
            catch (err) { expect(err.message).toBe("Hay parametros vacios o indefinidos"); }

            try { addProduct("", faker.commerce.product(), faker.image.imageUrl()) }
            catch (err) { expect(err.message).toBe("Hay parametros vacios o indefinidos"); }

            try { expect(addProduct(faker.commerce.product(), null , faker.image.imageUrl())); } 
            catch (err) { expect(err.message).toBe("Hay parametros vacios o indefinidos"); }
        });
        
        it("the response should be a error when the price has a non-numeric character", () => {
            expect.assertions(1);
            try { addProduct(faker.commerce.product(), faker.lorem.word, faker.image.imageUrl) }
            catch (err) { expect(err.message).toBe("El precio no puede contener caracteres no numericos") }
        })

        it("the response should be the product adding when parameters aren't missing", () => {
            products.map(e => { addProduct(e.title, e.price, e.thumbnail); });
            expect(getProducts()).toEqual(products);
        });

        it("should to assign the proper id", () => { 
            expect(getProducts()).toEqual(products);
        });
    })

    describe("controller: getProduct", () => {
        const id = Math.floor(Math.random() * (products.length + 1 - 1)) + 1;

        it(`the response should return the product with id: ${id}`, () => {
            expect(getProduct(id)).toEqual(products[id - 1]);
        });

        it("the response should be a error when hasn't none product with that id", () => {
            expect.assertions(1);
            try { getProduct(products.length + 1); }
            catch (err) { expect(err.message).toBe("Producto no encontrado") }
        })
    })

    describe("controller: updateProduct", () => {
        const id = Math.floor(Math.random() * (products.length + 1 - 1)) + 1;
        const newProduct = {
            id,
            title: faker.commerce.product(),
            price: parseFloat(faker.commerce.price()),
            thumbnail: faker.image.imageUrl()
        };

        it("the response should be a error when parameters are missing", () => {
            expect.assertions(1);
            try { updateProduct(id, faker.commerce.product(), faker.commerce.price()); }
            catch (err) { expect(err.message).toBe("Hay parametros vacios o indefinidos"); }
        })

        it("the response should be a error when the price has a non-numeric character", () => {
            expect.assertions(1);
            try { updateProduct(id, faker.commerce.product(), faker.lorem.word(), faker.image.imageUrl()); }
            catch (err) { expect(err.message).toBe("El precio no puede contener caracteres no numericos") }
        })

        it("the response should be a error when the id is non-numeric character", () => {
            expect.assertions(1);
            try { updateProduct(faker.lorem.word(), newProduct.title, newProduct.price, newProduct.thumbnail); }
            catch (err) { expect(err.message).toBe("El id del producto debe ser un caracter numerico"); }
        });

        it("the response should be a error when hasn't none product with that id", () => {
            expect.assertions(1);
            try { updateProduct(products.length + 1, newProduct.title, newProduct.price, newProduct.thumbnail) }
            catch (err) { expect(err.message).toBe(`No existe un producto con el id ${products.length + 1}`) }
        });

        it("the response should be the updated product", () => {
            const index = products.findIndex(e => e.id === id);
            products[index] = newProduct;
            expect(updateProduct(id, newProduct.title, newProduct.price, newProduct.thumbnail))
                .toEqual(newProduct);
        });
    })

    describe("controller: removeProduct", () => {
        const id = Math.floor(Math.random() * (products.length + 1 - 1)) + 1;

        it("the response should be a error when the id is a non-numeric character", () => {
            expect.assertions(1);
            try { removeProduct(faker.lorem.word); }
            catch (err) { expect(err.message).toBe("El id del producto debe ser un caracter numerico"); }
        });

        it("the response should be a error when hasn't none product with that id", () => {
            expect.assertions(1);
            try { removeProduct(products.length + 1); }
            catch (err) { expect(err.message).toBe(`No existe un producto con el id ${products.length + 1}`) }
        });

        it("the response should be the product removed", () => {
            const aux = products[id - 1]
            products.splice(id - 1, 1);
            expect(removeProduct(id)).toEqual(aux);
        });

        it("the listing should not has the product removed", () => {
            const reponse = getProducts();
            expect(reponse instanceof Array).toBeTruthy();
            expect(reponse).toEqual(products);
        })

    })
})