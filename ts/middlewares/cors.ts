import cors from 'cors';
import { ORIGIN_CLIENT } from '../config';

const configurationCors = {
  "origin": ORIGIN_CLIENT,
  "methods": "GET,HEAD,PUT,PATCH,POST,DELETE",
  "preflightContinue": false,
  "optionsSuccessStatus": 200
}

export default cors(configurationCors);
